import { declareFont, defineFont, Font, FontStyles, FontWeights } from '../utils/FontUtils'

enum FontKeys {
  Teko = 'Teko',
  BaunkRegular = 'BaunkRegular',
}

export const declarations: { [key in FontKeys]: Font } = {
  Teko: {
    basename: 'Teko-Regular',
    fontFamily: 'Teko',
    fontWeight: FontWeights.regular,
    fontStyle: FontStyles.normal,
    fallback: 'sans-serif',
  },
  BaunkRegular: {
    basename: 'Baunk-Regular',
    fontFamily: 'Baunk',
    fontWeight: FontWeights.regular,
    fontStyle: FontStyles.normal,
    fallback: 'serif',
  },
}

export const fontsFaces = (): string => Object.values(declarations).map(declareFont).join(' ')

const fonts: { [key in FontKeys]: string } = {
  Teko: defineFont(declarations.Teko),
  BaunkRegular: defineFont(declarations.BaunkRegular),
}

export default fonts
