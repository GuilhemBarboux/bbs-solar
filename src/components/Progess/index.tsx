import React, { FC } from 'react'
import { CircularProgress } from '@material-ui/core'

import * as SC from './styled'
import { ProgressProps } from './types'

const Progress: FC<ProgressProps> = ({ color }) => {
  return (
    <SC.Progress>
      <CircularProgress color={color || 'primary'} />
    </SC.Progress>
  )
}

export default Progress
