import { useEffect, useState } from 'react'

import { useSolarContext } from '../SolarProvider'

import { PlanetSearch, PlanetSearchProps } from './types'

const usePlanetSearch = (props: PlanetSearchProps): PlanetSearch => {
  const { searchPlanetByName } = useSolarContext()
  const [search, setSearch] = useState<PlanetSearch>({
    result: [],
    error: null,
    searching: false,
  })

  useEffect(() => {
    setSearch({
      result: [],
      error: null,
      searching: true,
    })

    searchPlanetByName(props.search)
      .then((r) =>
        setSearch({
          result: r.bodies || [],
          error: null,
          searching: false,
        })
      )
      .catch((e) =>
        setSearch({
          result: [],
          error: e,
          searching: false,
        })
      )
  }, [searchPlanetByName, setSearch, props.search])

  return search
}

export default usePlanetSearch
