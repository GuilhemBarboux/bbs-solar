import React from 'react'
import { Story } from '@storybook/react/types-6-0'

import defaultExport from '../../../.storybook/config/defaultExport'

import HomeTemplate, { HomeTemplateProps } from './index'
import { homeArgs } from './mocks'
import SolarProvider from '../../components/SolarProvider'
import { HelmetProvider } from 'react-helmet-async'

const ProviderTemplate = (StoryComponent: Story) => (
  <HelmetProvider>
    <SolarProvider>
      <StoryComponent />
    </SolarProvider>
  </HelmetProvider>
)

export default defaultExport({
  title: 'Templates/Home',
  component: HomeTemplate,
  decorators: [ProviderTemplate],
  parameters: {
    backgrounds: {
      default: 'light',
    },
  },
})

const Template: Story<HomeTemplateProps> = (args) => <HomeTemplate {...args} />

export const Default = Template.bind({})
Default.args = homeArgs
