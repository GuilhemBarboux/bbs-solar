import React, { FC, useEffect, useState } from 'react'
import { Input, InputLabel, Modal } from '@material-ui/core'
import { useHistory } from 'react-router-dom'

import PlanetList from '../../components/PlanetList'
import usePlanetSearch from '../../components/UsePlanetSearch'
import Progress from '../../components/Progess'
import PlanetDetail from '../../components/PlanetDetail'
import { Planet } from '../../components/SolarProvider/types'
import { Page } from '../../types/common'
import SEO from '../../components/SEO'

import * as SC from './styled'

export type HomeTemplateProps = Page & {
  title: string
  className?: string
  planet?: Planet | null
}

const HomeTemplate: FC<HomeTemplateProps> = (props) => {
  const { title, planet: currentPlanet } = props
  const [search, setSearch] = useState('')
  const [delay, setDelay] = useState(false)
  const { result, searching } = usePlanetSearch({
    search,
  })
  const history = useHistory()

  useEffect(() => {
    if (searching) {
      setDelay(true)
      setTimeout(() => setDelay(false), 700)
    }
  }, [searching, setDelay])

  return (
    <SC.Content>
      <SEO {...props.seo} />
      <SC.Title>{title}</SC.Title>
      <SC.Form fullWidth>
        <InputLabel htmlFor="standard-adornment-amount">Recherche</InputLabel>
        <Input
          id="standard-adornment-amount"
          value={search}
          onChange={(v) => setSearch(v.target.value)}
        />
      </SC.Form>
      {searching || delay ? <Progress /> : <PlanetList planets={result} />}
      <Modal
        open={!!currentPlanet}
        onClose={() => history.push('/')}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <SC.ModalContent>
          {currentPlanet ? <PlanetDetail planet={currentPlanet} /> : <Progress />}
        </SC.ModalContent>
      </Modal>
    </SC.Content>
  )
}

export default HomeTemplate
