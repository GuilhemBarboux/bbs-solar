import { HomeTemplateProps } from './index'

export const homeArgs: HomeTemplateProps = {
  title: 'Home',
  seo: {
    title: 'title',
    description: 'description',
  },
}
